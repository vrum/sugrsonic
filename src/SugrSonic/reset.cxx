#include <cassert>
#include "../SugrSonic.hxx"

void SugrSonic::reset(const int u_t[], const size_t nx_in, const size_t ny_in)
{
  assert(nx_in<=nx*values_per_register);
  assert(ny_in<=ny);
  std::vector<int16_t> temp(values_per_register);
  for (size_t y=0; y<ny_in; ++y)
    for (size_t x=0; x<nx; ++x)
      {
        u[x + nx*y].u_max=u[x + nx*y].u=u[x + nx*y].x=u[x + nx*y].y=
          _mm_setzero_si128();

        std::fill(temp.begin(), temp.end(), 0);
        size_t offset=x*values_per_register + nx_in*y;
        for (size_t dx=offset; dx<(y+1)*nx_in && dx<offset+values_per_register;
             ++dx)
          temp[dx-offset]=u_t[dx];

        u[x + nx*y].t=_mm_set_epi16(temp[7],temp[6],temp[5],temp[4],temp[3],
                                    temp[2],temp[1],temp[0]);
      }
}
                 
