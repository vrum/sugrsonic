#include "../SugrSonic.hxx"
#include "divide_by_2.hxx"

namespace
{
  inline __m128i plus_offset(const __m128i &plus, const __m128i &center)
  {
    __m128i shift_plus=_mm_slli_si128(plus,14);
    __m128i shift=_mm_srli_si128(center,2);
    return _mm_or_si128(shift_plus,shift);
  }

  inline __m128i new_t(const __m128i &x_plus, const __m128i &x,
                       const __m128i &y_plus, const __m128i &y,
                       const __m128i &t)
  {
    __m128i plus(plus_offset(x_plus,x));
    return _mm_add_epi16(t,divide_by_2(_mm_add_epi16(_mm_sub_epi16(plus,x),
                                                     _mm_sub_epi16(y_plus,y))));
  }

  inline __m128i new_u(const __m128i &t, const __m128i &u)
  {
    return _mm_add_epi16(u, divide_by_2(t));
  }
}

void SugrSonic::update_u_ut()
{
  U *center=u.data();
  U *x_plus=center+1;
  U *y_plus=center+nx;

  /// Update u_t and u
  for (size_t y=0; y<ny-1; ++y)
    {
      for (size_t x=0; x<nx-1; ++x)
        {
          center->t=new_t(x_plus->x, center->x, y_plus->y, center->y, center->t);
          center->u=new_u(center->t,center->u);
          center->u_max=_mm_max_epi16(center->u_max,_mm_abs_epi16(center->u));
          ++center;
          ++x_plus;
          ++y_plus;
        }
      /// x=nx-1
      center->t=new_t(_mm_setzero_si128(), center->x, y_plus->y, center->y,
                      center->t);
      center->u=new_u(center->t,center->u);
      center->u_max=_mm_max_epi16(center->u_max,_mm_abs_epi16(center->u));
      ++center;
      ++x_plus;
      ++y_plus;
    }
  /// y=ny-1
  for (size_t x=0; x<nx-1; ++x)
    {
      center->t=new_t(x_plus->x, center->x, _mm_setzero_si128(), center->y,
                      center->t);
      center->u=new_u(center->t,center->u);
      center->u_max=_mm_max_epi16(center->u_max,_mm_abs_epi16(center->u));
      ++center;
      ++x_plus;
      ++y_plus;
    }
  /// x=nx-1, y=ny-1
  center->t=new_t(_mm_setzero_si128(), center->x, _mm_setzero_si128(),
                  center->y, center->t);
  center->u=new_u(center->t,center->u);
  center->u_max=_mm_max_epi16(center->u_max,_mm_abs_epi16(center->u));
  ++center;
  ++x_plus;
  ++y_plus;
}
