#pragma once

#include <vector>
#include <smmintrin.h>

struct SugrSonic
{
  const size_t values_per_register=128/(sizeof(int16_t)*8);

  struct U
  {
    __m128i u_max, u, t, x, y, x_mask, y_mask;
  };

  const size_t nx, ny;
  std::vector<U> u;

  SugrSonic(const char x_mask[], const char y_mask[],
            const size_t NX, const size_t NY);

  void reset(const int u_t[], const size_t NX, const size_t NY);
  void step(const size_t &n)
  {
    for(size_t s=0; s<n; ++s)
      {
        update_u_ut();
        update_ux_uy();
      }
  }
  int16_t max_at(const size_t x_index, const size_t y)
  {
    size_t x=x_index/values_per_register;
    int16_t values[values_per_register];
    _mm_store_si128(reinterpret_cast<__m128i*>(values),u[x+nx*y].u_max);
    return values[x_index%values_per_register];
  }

  int16_t at(const size_t x_index, const size_t y)
  {
    size_t x=x_index/values_per_register;
    int16_t values[values_per_register];
    _mm_store_si128(reinterpret_cast<__m128i*>(values),u[x+nx*y].u);
    return values[x_index%values_per_register];
  }

  void update_u_ut();
  void update_ux_uy();
};
